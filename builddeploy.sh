#!/bin/sh
cd /vagrant/code/report-service
echo "Buildigng project"
ant -buildfile build.xml -Dant.build.javac.target="1.5" -Dant.build.javac.source="1.5"
echo "Shutting down tomcat server"
/usr/local/tomcat7/bin/shutdown.sh
echo "Removing old report service"
sudo rm /usr/local/tomcat7/webapps/report-service* -rf
echo "Deploying new built war"
cp ./build/report-service.war  /usr/local/tomcat7/webapps/report-service.war
echo "Restart tomcat server"
/usr/local/tomcat7/bin/startup.sh
echo "Done."
