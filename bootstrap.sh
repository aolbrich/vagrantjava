#!/usr/bin/env bash

#Global vars
CONFIG_PATH='/vagrant/config';
CODE_DIR='/vagrant/code';

echo "*** Check if config file exists and contains all required vars"
if [ ! -f $CONFIG_PATH ]; then cat /vagrant/README; exit 1; fi #Check if file exist
source $CONFIG_PATH #Ingest the config key=value pairs

#Create a file contianing all globals (needed when calling su varrant -c 'source /vagrant/globals')
cat $CONFIG_PATH > /vagrant/globals && echo -e "CODE_DIR=$CODE_DIR\nSUBDOMAIN=$SUBDOMAIN\nCONFIG_PATH=$CONFIG_PATH" >> /vagrant/globals;

echo "*** Disable SELinux enforcement" && \
(getenforce | egrep -c "^Disabled$" || (sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config && setenforce 0)) && \

echo "*** Install packages required for app running (10 min)"
yum update -y && \

yum install -y memcached openssl && \ #needed for app run
yum install -y gcc git ansible libselinux-python dos2unix ruby ruby-el rubygems ruby-devel nodejs && \ #needed for build and dev
echo "*** Install JDK ***"
yum install -y java-1.6.0-openjdk.x86_64 java-1.6.0-openjdk-devel.x86_64 && \

echo "*** Install Tomcat7 ***"
mkdir -p /usr/local/tomcat7 && \
chown vagrant:vagrant  /usr/local/tomcat7 -R && chmod +x /vagrant/builddeploy.sh && \
wget http://apache.mirrors.nublue.co.uk/tomcat/tomcat-7/v7.0.81/bin/apache-tomcat-7.0.81.tar.gz -O /tmp/tomcat7.tar.gz && \
echo "*** Extracting Tomcat7 ***"
tar -xvzf /tmp/tomcat7.tar.gz -C /usr/local/tomcat7 --strip-components=1 && \

echo "*** Adding oracle odbc extention to tomcat"
cp /vagrant/tomcatSettings/lib/ojdbc14.jar  /usr/local/tomcat7/lib/ojdbc14.jar && \
cp /vagrant/tomcatSettings/lib/tomcat7-websocket.jar  /usr/local/tomcat7/lib/tomcat7-websocket.jar && \

echo "*** Install ant ***"
yum install -y ant && \

echo "*** Create Tomcat settings for cas and report-service ***"
cp /vagrant/tomcatSettings/conf/* /usr/local/tomcat7/conf/ -R && \

echo "*** Install hosts ***"
cat /vagrant/etc-hosts >> /etc/hosts && \

echo "*** Checkout and build the code (takes 20 minutes)" && \
mkdir -p /usr/share/php/Zend && chown -R vagrant:vagrant /usr/share/php/Zend && \
su vagrant -c '
	source /vagrant/globals && \
	([ -d $CODE_DIR/report-service/.git ] || git clone https://$git_user:$git_password@bitbucket.org/shipserv/report-service.git $CODE_DIR/report-service)
'
